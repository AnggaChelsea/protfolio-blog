import { Component, Inject, OnDestroy, OnInit, ViewEncapsulation, PLATFORM_ID } from '@angular/core';
import { CommonModule, isPlatformBrowser } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavbarComponent implements OnInit {
  isAuthenticated = false;
  userSub!: Subscription;
  widthScreen = 0;
  fontSize: any;
  buttonColor = false;
  className!: string;

  constructor(private authService: AuthService, @Inject(PLATFORM_ID) private platformId: any) {
    if(isPlatformBrowser(this.platformId)){
      this.widthScreen = window.innerWidth
    }

    console.log(this.widthScreen,'width screen')
   }

  ngOnInit(): void {
   this.userSub = this.authService.data$.subscribe(user => {
    this.isAuthenticated = user ? true : false; //!user ? true : false
    console.log(`isAuthenticated: ${this.isAuthenticated}`)
   })
   this.widthScreen > 1280 ? this.fontSize = '60px': this.fontSize = '40px'
  }

  changeColor(){
    this.buttonColor = !this.buttonColor;
  }
  doChange(color: string){
    switch(color){
      case 'dark':
      this.className = 'dark';
      break;
      case 'white':
        this.className = 'white';
        break;
      case 'blue':
        this.className = 'blue';
        break;
        default:
    }
    console.log(this.className);
  }

  // ngOnDestroy(): void {
  //     this.authService.user.unsubscribe()
  // }

}
