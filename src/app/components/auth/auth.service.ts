import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs'
import { catchError, tap } from 'rxjs/operators'
import { environment } from 'src/environments/environment'
import { UserModel } from './auth_user.model'

export interface AuthDataResponse {
  // kind: string,
  idToken: string
  email: string
  refreshToken: string
  expiresIn: string
  localId: string
}

export interface AuthLoginResponse {
  kind: string
  accessToken: string
  expiresIn: string
}



@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user = new BehaviorSubject<any>(null)
  data$ = this.user.asObservable()
  constructor(private http: HttpClient) {}

  signup(email: string, password: string): Observable<AuthDataResponse> {
    return this.http
      .post<AuthDataResponse>(`${environment.apiAuthFirebase}`, {
        email: email,
        password: password,
        returnSecureToken: true,
      })
      .pipe(
        catchError(this.errorHandler),
        tap((resData) => {
         this.errorAuthentication(
          resData.email, resData.localId, resData.idToken, resData.expiresIn
         )
        }),

      )

  }

  signin(email: string, password: string) {
    return this.http
      .post<AuthDataResponse>(`${environment.apiAuthLogin}`, {
        email: email,
        password: password,
        returnSecureToken: true,
      })
      .pipe(catchError(this.errorHandler))
  }

  private errorAuthentication(email: string, localId: string, idToken: string, expiresIn: string){
    const expiratoinDate = new Date(
      new Date().getTime() + +expiresIn * 1000,
    )
    const user = new UserModel(
      email,
      localId,
      idToken,
      expiratoinDate,
    );
    this.user.next(user)
  }

  private errorHandler(errRes: HttpErrorResponse) {
    let errorMessage = 'uknown error'
    if (!errRes.error || errRes.error.errors) {
      return throwError(errorMessage)
    }
    switch (errRes.error.errors.message) {
      case 'EMAIL_EXISTS':
        errorMessage = 'email already exist'
        break
      case 'EMAIL_NOT_FOUND':
        errorMessage = 'email not found'
        break
      case 'INVALID_PASSWORD':
        errorMessage = 'invalid password'
        break
    }
    return throwError(errorMessage)
  }
}
