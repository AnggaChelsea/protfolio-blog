import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core'
import { CommonModule, ViewportScroller, isPlatformBrowser } from '@angular/common'
import { FormsModule, NgForm } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { AuthDataResponse, AuthService } from './auth.service';
import { SpinnerComponent } from 'src/app/shared/spinner/spinner.component';
import { Router, RouterModule } from '@angular/router';
import { Observable } from 'rxjs';


const firebaseConfig = {
  apiKey: "AIzaSyDm9Kmf-F2-hQL5QrwtwI--9LKHz4U64Mo",
  authDomain: "auth-59b16.firebaseapp.com",
  databaseURL: "https://auth-59b16.firebaseio.com",
  projectId: "auth-59b16",
  storageBucket: "auth-59b16.appspot.com",
  messagingSenderId: "322098566281",
  appId: "1:322098566281:web:b5f07e359e7448cf2ce258"
};

@Component({
  selector: 'app-auth',
  standalone: true,
  imports: [CommonModule, RouterModule, FormsModule, HttpClientModule,SpinnerComponent ],
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  isLoginMode = true;
  isError = false;
  isLoading = false;
  loadError = false;
  message: any;
  constructor(private authService: AuthService, private router: Router, private viewPort: ViewportScroller, @Inject(PLATFORM_ID) private platformId: any) {
    if (isPlatformBrowser(this.platformId)) {
      const width = window.innerWidth;
      const height = window.innerHeight;
      console.log(width + 'x' + height);
  }
  }

  ngOnInit(): void {
    console.log(window.screen.width, 'screen windows')
    console.log(this.isLoginMode)
  }

  // async loginWithGoogle(){
  //   const provider = new auth.GoogleAuthProvider();
  //   const credential = await this.afAuth.signInWithPopup(provider);
  //   console.log(credential.user);
  // }

  switchMode() {
    this.isLoginMode = !this.isLoginMode;
  }

  onSubmit(form: NgForm){
    this.isLoading = true;
    if(!form.valid) return;
    const email = form.value.email;
    const password = form.value.password

    let authObs : Observable<AuthDataResponse>

    console.log(email)
    const data = "test";
    if(this.isLoginMode) {
      authObs = this.authService.signin(email, password)
    }else{
      authObs = this.authService.signup(email, password)
    }

    //handle the subscribe with authObs Observable and make it readable didnt code same
    authObs.subscribe((value: any) => {
      this.message = 'success login';
      console.log(value);
      this.isLoading = false;
      this.router.navigate([''])
    }, (resData: any) => {
      this.isLoading = false;
      console.log(resData);
      this.message = resData;
      this.isError = true;
      this.loadError = true
      setTimeout(() => {
        this.loadError = false;
      }, 5000)
    })
    form.reset()
  }

}

