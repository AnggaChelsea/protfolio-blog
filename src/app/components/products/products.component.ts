import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-products',
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
    <h1 class="text-3xl font-bold underline">
  Hello world!
</h1>
    </p>
  `,
  styles: [
  ]
})
export class ProductsComponent implements OnInit {

  product = [
    {
      title: 'product a',
      price: 250,
      decsription: 'product a description adalah product best product',
    },
    {
      title: 'product a',
      price: 250,
      decsription: 'product a description adalah product best product',
    },
    {
      title: 'product a',
      price: 250,
      decsription: 'product a description adalah product best product',
    },
    {
      title: 'product a',
      price: 250,
      decsription: 'product a description adalah product best product',
    },
  ]

  constructor() { }

  ngOnInit(): void {
  }

  //create get data
  getData() {
    return this.product;
  }

}
